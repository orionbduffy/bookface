import React from 'react';
import EmailAddressInput from '../input/EmailAddressInput.js';
import PasswordInput from '../input/PasswordInput.js';
import UsernameInput from '../input/UsernameInput.js';
import Button from '@material-ui/core/Button';
import {Redirect} from 'react-router-dom';
import {withRouter} from 'react-router-dom';
import '../styles/LoginPage.css';

class RegisterPage extends React.Component {

    render(){
        const {username, emailAddress, password, handleEmailAddressChange,
               handlePasswordChange, handleUsernameChange, onRegisterClick} = this.props;
        return(
            <div className = "LoginInputContainer">
                <div className = "Inputs">
                    <EmailAddressInput emailAddress = {emailAddress} onEmailChange = {handleEmailAddressChange}/>
                </div>
                <div className = "Inputs">
                    <UsernameInput userName = {username} onUsernameChange = {handleUsernameChange} />
                </div>
                <div className = "Inputs">
                    <PasswordInput password = {password} onPasswordChange = {handlePasswordChange} />
                </div>
                <div className = "Inputs">
                    <Button variant = "contained" onClick = {onRegisterClick}> Register </Button>
                </div>
            </div>
        );
    }
}
export default withRouter(RegisterPage);