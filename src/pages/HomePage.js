import React from 'react';
import Posts from "../scripts/posts";
import {withRouter, Redirect} from 'react-router-dom';

class HomePage extends React.Component{

    render(){
        const {user, redirectToLogin} = this.props;
        if (redirectToLogin){ //keeps access restricted to logged in users.
            return <Redirect to = "/" />;
        }
        const {username} = user;
        return(
            <Posts username={username}/>
        );
    }
}

export default withRouter(HomePage);