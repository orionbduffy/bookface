import React from 'react';
import EmailAddressInput from '../input/EmailAddressInput.js';
import PasswordInput from '../input/PasswordInput.js';
import Button from '@material-ui/core/Button';
import {withRouter} from 'react-router';
import {Link, Redirect} from 'react-router-dom';
import '../styles/LoginPage.css';

class LoginPage extends React.Component {

    render(){
        const {emailAddress, password, redirectToLogin, onLoginClick,
               handleEmailAddressChange, handlePasswordChange} = this.props;
        if (!redirectToLogin){
            return <Redirect to = "/home" />;
        }
        return(
             <div className = "LoginInputContainer">
                 <div className = "Inputs">
                     <EmailAddressInput emailAddress = {emailAddress} onEmailChange = {handleEmailAddressChange}/>
                 </div>
                 <div className = "Inputs">
                     <PasswordInput password = {password} onPasswordChange = {handlePasswordChange} />
                 </div>
                 <div className = "Inputs">
                     <Button variant = "contained" onClick = {onLoginClick}> Login </Button>
                 </div>
                 <p> Not a user? <Link to = "/register"> Sign up! </Link> </p>
            </div>
        );
    }
}
export default withRouter(LoginPage);