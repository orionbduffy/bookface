import React from 'react';
import {withRouter, Redirect} from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import '../styles/ProfilePage.css';

class ProfilePage extends React.Component {

    render(){
        const {user, redirectToLogin} = this.props;
        if (redirectToLogin){
            return <Redirect to = "/" />;
        }
        const {username: name} = user;
        return <div className = "profilePage">
                    <div className = "profileBody">
                        <Paper>
                            <Typography variant = "body1">
                                Name: {name}
                            </Typography>
                        </Paper>
                    </div>
               </div>;
    }

}

export default withRouter(ProfilePage);