import React from 'react';
import '../styles/Posts.css';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import SendPost from "../input/SendPost";
import FavoriteCheck from "../input/FavoriteCheck";


const post = (username, date, message) => {
    return {username: username, date: new Date(date), message:message};
};

const createNewPost = (username, message) => {
    return {username: username, date: new Date(), message:message};
};

const posts = [post('God', '2018-04-27T00:00:00', 'Hello, mortals. Welcome to my realm.'),
        post('SteveJobs', '2018-04-27T06:16:26', 'Who are you?'),
        post('God', '2018-04-28T00:00:00', 'I am the one who balances all things, exactly as they should be.'),
        post('BillGates', '2018-04-28T03:13:23', 'Is he the admin?'),
        post('SteveJobs', '2018-04-28T06:16:26', 'Does that matter? I\'m going to steal this idea and make a better version soon enough.'),
        post('BillGates', '2018-04-29T03:13:23', 'You know Steve, you can\'t steal everything... One day, that\'s going to cause you issues.'),
        post('God', '2018-04-30T00:00:00', 'I stole half the universe, and it hasn\'t caused me any problems yet.'),
        post('BillGates', '2018-04-30T03:13:23', 'Just wait almost exactly 1 year, and it will.')];

class Posts extends React.Component {
    constructor(props){
        super(props);
        this.state = {posts: posts, postDraft: "Please type message here..."};

        this.handlePostDraftChange = this.handlePostDraftChange.bind(this);
        this.handlePostButtonClick = this.handlePostButtonClick.bind(this);
        this.postForm = this.postForm.bind(this);
    }

    postForm (post)
    {
        return (
            <Grid item xs={12}>
                <Paper className={"postPaper"}>
                    <Grid item>
                        <div className={"postDiv"}>
                            <div className={"postInfo"}>
                                <h5>
                                    <span className={"nameStyle"}>{post.username.valueOf()}</span>
                                    <span className={"dateStyle"}>{post.date.toLocaleString()}</span>
                                </h5>
                            </div>
                            <div>
                                <p>{post.message.valueOf()}</p>
                            </div>
                            <div><FavoriteCheck/></div>
                        </div>
                    </Grid>
                </Paper>
            </Grid>
        );
    };

    handlePostDraftChange (event)
    {
        this.setState({postDraft: event.target.value});
    }

    handlePostButtonClick (event)
    {
        const {username} = this.props;
        const {postDraft} = this.state;
        const newPost = createNewPost(username, postDraft);
        this.setState({posts: [...this.state.posts, newPost]});
    }

        render () {
                const combinedPosts = this.state.posts.map(this.postForm);
                return (
                    <div className={"mainDiv"}>
                    <h2>Posts</h2>
                    <div>{combinedPosts}</div>
                    <SendPost postDraft = {this.state.postDraft} onPostDraftChange = {this.handlePostDraftChange} onPostButtonClick = {this.handlePostButtonClick} />
                    </div>
                );
        };
}
export default Posts;