const user = (username, emailAddress, password, creationDate) => {
    return {username:username, emailAddress: emailAddress, password:password, creationDate:new Date(creationDate), likes: 0, dislikes: 0}
};


const users =  [user('God', 'Thanos@Rockly.com', 'Jesus', '2012-05-04T00:00:00'),
                user('BillGates', 'BillGates@microsoft.com', 'Microsoft', '2013-03-13T03:13:23'),
                user('SteveJobs', 'SteveJobs@apple.com', 'Apple', '2016-06-16T06:16:26')];