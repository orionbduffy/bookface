import React from 'react';
import '../styles/App.css';
import LoginPage from '../pages/LoginPage.js'
import RegisterPage from '../pages/RegisterPage.js'
import HomePage from '../pages/HomePage.js'
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import NavigationBar from "./NavigationBar";
import { ThemeProvider } from '@material-ui/styles';
import blue from "@material-ui/core/colors/blue";
import ProfilePage from "../pages/ProfilePage";

const theme = createMuiTheme({
  palette: {
    primary: {main: '#2196f3'},
    secondary: {main: '#42a5f5'},
  },
});

const user = (username, emailAddress, password, creationDate) => {
  return {username:username, emailAddress: emailAddress, password:password, creationDate:new Date(creationDate), likes: 0, dislikes: 0}
};


const users =  [user('God', 'Thanos@Rockly.com', 'Jesus', '2012-05-04T00:00:00'),
  user('BillGates', 'BillGates@microsoft.com', 'Microsoft', '2013-03-13T03:13:23'),
  user('SteveJobs', 'SteveJobs@apple.com', 'Apple', '2016-06-16T06:16:26')];

const registerUser = (username, emailAddress, password) => {

  return {username:username, emailAddress: emailAddress, password:password, creationDate:new Date(), likes: 0, dislikes: 0}
};

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username : "default",
      emailAddress : "default@default.com",
      password : "password",
      users: users,
      user: registerUser("","",""),
      redirectToLogin: true
    };
    this.handleEmailAddressChange = this.handleEmailAddressChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.onRegisterClick = this.onRegisterClick.bind(this);
    this.onLoginClick = this.onLoginClick.bind(this);
    this.onLogoutClick = this.onLogoutClick.bind(this);
  }

  handleEmailAddressChange(event) {
    const newEmailAddress = event.target.value;
    this.setState({ emailAddress: newEmailAddress });
  }

  handlePasswordChange(event) {
    const newPassword = event.target.value;
    this.setState({ password: newPassword });
  }

  handleUsernameChange(event) {
    const newUsername = event.target.value;
    this.setState({username: newUsername});
  }

  onLoginClick(){
    const {emailAddress, password, users} = this.state;
    if(users.length !== 0) {
      const userExists = users.filter( (existingUser) => {
        return existingUser.emailAddress === emailAddress && existingUser.password === password;
      });
      if (userExists.length === 1) {
        const newUser = userExists[0];
        this.setState({user: newUser, redirectToLogin: false});
      }
      else {
        alert("The email address or password was incorrect.");
      }
    }
  }

  onLogoutClick(){
    this.setState({
      user: registerUser("","",""),
      redirectToLogin: true
    });
  }

  onRegisterClick(){
    const {emailAddress, password, users, username} = this.state;
    const user = registerUser(username, emailAddress, password);
    const duplicateUsers = users.filter((user) => {
      return user.emailAddress === emailAddress
    });
    if(duplicateUsers.length === 0){
      this.setState({users: [...this.state.users, user]});
    }
    else{
      alert("That user already exists.");
    }
  }

  render(){
    const {username, emailAddress, password, user, redirectToHome, redirectToLogin} = this.state;
    return (
        <ThemeProvider theme={theme}>
          <BrowserRouter>
            <NavigationBar user = {user} onLogout = {this.onLogoutClick}/>
            <Switch>
              <Route exact path = "/">
                <LoginPage
                    emailAddress = {emailAddress}
                    password = {password}
                    handleEmailAddressChange = {this.handleEmailAddressChange}
                    handlePasswordChange = {this.handlePasswordChange}
                    redirectToLogin = {redirectToLogin}
                    onLoginClick = {this.onLoginClick}
                />
              </Route>
              <Route path = "/register">
                <RegisterPage
                    username = {username}
                    emailAddress = {emailAddress}
                    password = {password}
                    handleUsernameChange = {this.handleUsernameChange}
                    handleEmailAddressChange = {this.handleEmailAddressChange}
                    handlePasswordChange = {this.handlePasswordChange}
                    onRegisterClick = {this.onRegisterClick}
                />
              </Route>
              <Route path = "/home">
                <HomePage user = {user} redirectToLogin = {redirectToLogin}/>
              </Route>
              <Route path = "/profile">
                <ProfilePage user = {user} redirectToLogin = {redirectToLogin} />
              </Route>
            </Switch>
          </BrowserRouter>
        </ThemeProvider>
    );
  }

}

export default App;