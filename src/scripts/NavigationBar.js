import React from 'react';
import Button from "@material-ui/core/Button";
import {Link, withRouter} from 'react-router-dom';
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Toolbar from "@material-ui/core/Toolbar";
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import AppBar from "@material-ui/core/AppBar";

class NavigationBar extends React.Component {

    constructor(props) {
        super(props);

        this.handleHomePageNavigation = this.handleHomePageNavigation.bind(this);
        this.handleLoginPageNavigation = this.handleLoginPageNavigation.bind(this);
        this.handleRegisterPageNavigation = this.handleRegisterPageNavigation.bind(this);
        this.handleProfilePageNavigation = this.handleProfilePageNavigation.bind(this);
        this.loggedInNavBar = this.loggedInNavBar.bind(this);
        this.loggedOutNavBar = this.loggedOutNavBar.bind(this);
        this.handleLogoutPageNavigation = this.handleLogoutPageNavigation.bind(this);
    }

    loggedInNavBar(){
        const {user} = this.props;
        const name = user.username;

        return (

            <AppBar position={"static"}>

                <Toolbar>

                    <IconButton edge={"start"} color={"inherit"} onClick={this.handleProfilePageNavigation}>
                        <AccountBoxIcon />
                    </IconButton>

                    { /* ToolBar uses a flex layout so we can use flexGrow here to ensure it pushes buttons all the way to the right */ }
                    <Typography variant={"h6"} style={{ flexGrow: 1 }}>
                        Welcome {name}
                    </Typography>

                    <Button color={"inherit"}
                            onClick={this.handleHomePageNavigation}
                            style={{ marginRight: '1rem' }}>
                        Home
                    </Button>

                    <Button color={"inherit"}
                            onClick={this.handleLogoutPageNavigation}>
                        Logout
                    </Button>

                </Toolbar>
            </AppBar>
        );
    }

    loggedOutNavBar(){
        return (

            <AppBar position={"static"}>

                <Toolbar>

                    <Typography variant={"h6"} style={{ flexGrow: 1 }}>
                        Welcome to BookFace!
                    </Typography>

                    <Button color={"inherit"}
                            onClick={this.handleLoginPageNavigation}>
                        Login
                    </Button>

                    <Button color={"inherit"}
                            onClick={this.handleRegisterPageNavigation}>
                        Register
                    </Button>

                </Toolbar>
            </AppBar>
        );
    }

    isLoggedIn () {
        const {user} = this.props;
        const name = user.username;
        if(name.length > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    handleProfilePageNavigation() {
        if (this.isLoggedIn()) {
            const {history} = this.props;
            history.push('/profile');
        }
    }

    handleHomePageNavigation() {
        if (this.isLoggedIn()) {
            const {history} = this.props;
            history.push('/home');
        }
    }

    handleLoginPageNavigation() {
        if(!this.isLoggedIn()) {
            const {history} = this.props;
            history.push('/');
        }
    }

    handleLogoutPageNavigation() {
        const { history, onLogout} = this.props;
        onLogout();
        history.push('/');
    }

    handleRegisterPageNavigation() {
        if(!this.isLoggedIn()) {
            const {history} = this.props;
            history.push('/register');
        }
    }

    render () {
        if (this.isLoggedIn()){
            return this.loggedInNavBar();
        }
        else {
            return this.loggedOutNavBar();
        }

        // return (
        //     <Router>
        //     <div>
        //         <BottomNavigation
        //             value={value}
        //             onChange={(event, newValue) => {
        //                 setValue(newValue);
        //             }}
        //             showLabels
        //             className={classes.root}
        //         >
        //             <BottomNavigationAction label="Recents" icon={<RestoreIcon />} />
        //             <BottomNavigationAction label="Favorites" icon={<FavoriteIcon />} />
        //             <BottomNavigationAction label="Nearby" icon={<LocationOnIcon />} />
        //         </BottomNavigation>
        //         <Button variant="contained" href="/register" color = "primary" className="">
        //             Register
        //         </Button>
        //         <Link to = "/register"> Register </Link>
        //     </div>
        //     </Router>
        // );
    }

}


export default withRouter(NavigationBar);