import React from 'react';
import TextField from '@material-ui/core/TextField'

class EmailAddressInput extends React.Component {

    render() {
        const {emailAddress, onEmailChange} = this.props;
        return <TextField label = "Email Address" value = { emailAddress } onChange = { onEmailChange } />;
    }

}

export default EmailAddressInput;