import React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import '../styles/SendPost.css';

class SendPost extends React.Component {
    render() {
        const {postDraft, onPostDraftChange, onPostButtonClick} = this.props;
        return(
            <div className ='sendPostDiv'>
            <TextField
                label="Post Reply"
                multiline
                rowsMax={"5"}
                className= ""
                value= {postDraft}
                onChange={onPostDraftChange}
                margin="normal"
                variant="outlined"
            />
            <Button variant="contained" className={""} onClick={onPostButtonClick}>
            Post Reply
            </Button>
            </div>
        );
    }
}

export default SendPost;