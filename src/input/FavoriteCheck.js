import React from 'react';

import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import ThumbDownAltIcon from '@material-ui/icons/ThumbDownAlt';

class FavoriteCheck extends React.Component {
    constructor(props){
        super(props);
        this.state = {likeclicks: 0, dislikeclicks: 0};

        this.handlelikeClick = this.handlelikeClick.bind(this)
        this.handledislikeClick = this.handledislikeClick.bind(this)
    }

    handlelikeClick () 
    {
        const { likeclicks } = this.state;

        this.setState({ likeclicks:  likeclicks  + 1 } );
    }

    handledislikeClick () 
    {
        const { dislikeclicks } = this.state;

        this.setState({ dislikeclicks:  dislikeclicks  + 1 } );
    }

    render () {
            const likeicon = this.state.likeclicks ? <ThumbUpAltIcon fontSize={"large"}></ThumbUpAltIcon> : <ThumbUpAltIcon fontSize={"medium"}></ThumbUpAltIcon>
            const dislikeicon = this.state.dislikeclicks ? <ThumbDownAltIcon fontSize={"large"}></ThumbDownAltIcon> : <ThumbDownAltIcon fontSize={"medium"}></ThumbDownAltIcon>
            const { likeclicks } = this.state;
            const { dislikeclicks } = this.state;
            return (
                <div>
                <button onClick={this.handlelikeClick.bind(this)} style={{ border: 'none', backgroundColor: 'white', outline: 'none', color: 'red' }}>{likeicon}{likeclicks}</button>
                
                <button onClick={this.handledislikeClick.bind(this)} style={{ border: 'none', backgroundColor: 'white', outline: 'none', color: 'red' }}>{dislikeicon}{dislikeclicks}</button>
                </div>
            );
    };
}
export default FavoriteCheck;