import React from 'react';
import TextField from '@material-ui/core/TextField'

class PasswordInput extends React.Component {

    render() {
        const {password, onPasswordChange} = this.props;
        return <TextField label = "Password" value = { password } onChange = { onPasswordChange } type = "password" />;
    }

}

export default PasswordInput;