import React from 'react';
import TextField from '@material-ui/core/TextField'

class UsernameInput extends React.Component {

    render() {
        const {username, onUsernameChange} = this.props;
        return <TextField label = "Username" value = { username } onChange = { onUsernameChange } />;
    }

}

export default UsernameInput;